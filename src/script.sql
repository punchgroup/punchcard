create table usuario
(
    id_usuario varchar(60) not null
        constraint usuario_pk
            primary key,
    correo varchar(50),
    password varchar(60)
);

create unique index usuario_correo_uindex
    on usuario (correo);

create table partida
(
    id_partida varchar(60) not null
        constraint partida_pk
            primary key,
    id_usuario varchar(60)
        constraint usuario_fk
            references usuario,
    nivel integer,
    puntaje integer
);


create table datos
(
    id_usuario varchar(60)
        constraint data_usuario_id_usuario_fk
            references usuario,
    puntaje_total integer,
    primary key (id_usuario)
);

