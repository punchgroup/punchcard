//Todas las cartas en un arreglo
let typeCards = [
   { name: "cA", image: "images/carta/corazon/A.png" },
   { name: "eA", image: "images/carta/espada/A.png" },
   { name: "tA", image: "images/carta/trebol/A.png" },
   { name: "dA", image: "images/carta/diamante/A.png" },

   { name: "c2", image: "images/carta/corazon/2.png" },
   { name: "e2", image: "images/carta/espada/2.png" },
   { name: "t2", image: "images/carta/trebol/2.png" },
   { name: "d2", image: "images/carta/diamante/2.png" },

   { name: "c3", image: "images/carta/corazon/3.png" },
   { name: "e3", image: "images/carta/espada/3.png" },
   { name: "t3", image: "images/carta/trebol/3.png" },
   { name: "d3", image: "images/carta/diamante/3.png" },

   { name: "c4", image: "images/carta/corazon/4.png" },
   { name: "e4", image: "images/carta/espada/4.png" },
   { name: "t4", image: "images/carta/trebol/4.png" },
   { name: "d4", image: "images/carta/diamante/4.png" },

   { name: "c5", image: "images/carta/corazon/5.png" },
   { name: "e5", image: "images/carta/espada/5.png" },
   { name: "t5", image: "images/carta/trebol/5.png" },
   { name: "d5", image: "images/carta/diamante/5.png" },

   { name: "c6", image: "images/carta/corazon/6.png" },
   { name: "e6", image: "images/carta/espada/6.png" },
   { name: "t6", image: "images/carta/trebol/6.png" },
   { name: "d6", image: "images/carta/diamante/6.png" },

   { name: "c7", image: "images/carta/corazon/7.png" },
   { name: "e7", image: "images/carta/espada/7.png" },
   { name: "t7", image: "images/carta/trebol/7.png" },
   { name: "d7", image: "images/carta/diamante/7.png" },

   { name: "c8", image: "images/carta/corazon/8.png" },
   { name: "e8", image: "images/carta/espada/8.png" },
   { name: "t8", image: "images/carta/trebol/8.png" },
   { name: "d8", image: "images/carta/diamante/8.png" },

   { name: "c9", image: "images/carta/corazon/9.png" },
   { name: "e9", image: "images/carta/espada/9.png" },
   { name: "t9", image: "images/carta/trebol/9.png" },
   { name: "d9", image: "images/carta/diamante/9.png" },

   { name: "c10", image: "images/carta/corazon/10.png" },
   { name: "e10", image: "images/carta/espada/10.png" },
   { name: "t10", image: "images/carta/trebol/10.png" },
   { name: "d10", image: "images/carta/diamante/10.png" },

   { name: "cJ", image: "images/carta/corazon/J.png" },
   { name: "eJ", image: "images/carta/espada/J.png" },
   { name: "tJ", image: "images/carta/trebol/J.png" },
   { name: "dJ", image: "images/carta/diamante/J.png" },

   { name: "cQ", image: "images/carta/corazon/Q.png" },
   { name: "eQ", image: "images/carta/espada/Q.png" },
   { name: "tQ", image: "images/carta/trebol/Q.png" },
   { name: "dQ", image: "images/carta/diamante/Q.png" },

   { name: "cK", image: "images/carta/corazon/K.png" },
   { name: "eK", image: "images/carta/espada/K.png" },
   { name: "tK", image: "images/carta/trebol/K.png" },
   { name: "dK", image: "images/carta/diamante/K.png" },
];

function getRandom(arr, n) {
   var result = new Array(n),
      len = arr.length,
      taken = new Array(len);
   if (n > len)
      throw new RangeError("getRandom: more elements taken than available");
   while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
   }
   return result;
}

// x el numero de cartas que tomaremos y barajearemos del mazo
function shuffleCards(x) {
   let chosenCards = getRandom(typeCards, x);
   let chosen = [].concat(_.cloneDeep(chosenCards), _.cloneDeep(chosenCards));
   return _.shuffle(chosen);
}

var app = new Vue({
      el: '#app',
      data: {
         iniciar: false,
         notReg: false,
         titulo: 'Registro de Usuario',
         pagina: 1,//pagina inicial
         usuario: {// para el registro
            idUsuario: null,
            correo: null,
            password: null
         },

         login: {
            correo: null,
            password: null
         },
         usuarioF: {//Para las partidas
            idUsuario: null,
            correo: null,
            password: null
         },
         datos: {
            usuarioF: {
               idUsuario: null,
            },
            puntaje_total: null,
            nivel: null,
         },
         partida: {
            idPartida: null,
            usuarioF: {
               idUsuario: null,
               correo: null,
               password: null,
            },
            master: null,
            estado: null,
         },
         /**********************************/
         nivel: 1,
         nivelMaximo: 1,
         niveles: [],
         showSplash: false,
         cartas: [],
         started: false,
         startTime: 0,
         turns: 0,
         flipBackTimer: null,
         timer: null,
         time: "--:--",
         score: 0,
         partidas: [],
         puntajeTotal: 0,
         ranks: [],
         isActive: false,
         mensaje: null,
         show:true,
         cantidad:4,
      },

      methods: {
         launch: function() {
            this.isActive = true;
         },
         close: function() {
            this.isActive = false;
         },
         fechaActual: function (date) {
            return moment(date).format('MMMM Do YYYY, h:mm:ss a');
         },
         empezar: function () {
            return (this.iniciar == true)
         },
         mostrarNotReg: function () {
            return (this.notReg == true);
         },
         setPagina: function (pagina) {
            this.pagina = pagina;
         },
         isPagina: function (pagina) {
            return (this.pagina == pagina);
         },
         // Si la sesión está iniciada, nos lleva a la pagina de menu
         checkSession: function () {
            let data = window.localStorage.getItem("user");
            if (data) {
               this.usuarioF = JSON.parse(data);
               this.pagina = 4;
               this.iniciar = true;
            } else {
               this.pagina = 2;
            }
         },
         // Cierra la sesión
         closeSession: function () {
            window.localStorage.removeItem("user");
            this.pagina = 2;
            this.iniciar = false;
            this.usuarioF.idUsuario = null;
            this.usuarioF.correo = null;
            this.usuarioF.password = null;

         },
         // Registra un usuario nuevo
         registrarUsuarioF: function () {
            let self = this;
            fetch('registrar-usuariof', {
               method: 'POST',
               body: JSON.stringify(this.usuario),
               headers: {
                  'Content-Type': 'application/json'
               }
            }).then(function (res) {
               return res.json();
            }).then(function (data) {
               console.log(data)
               if (data) {
                  self.mensaje="Se ha registrado correctamente";
                  self.isActive = true;
                  self.pagina = 2;
                  self.notReg = true;

               } else {
                  self.mensaje="Ha ocurrido un error. \n Puede que el usuario o correo ya ha sido registrado";
                  self.isActive = true;
               }
            });
         },
         // Consulta el máximo nivel desbloqueado por el jugador
         consultarNivelMaximo: function () {
            if (!this.usuarioF) return;
            fetch('progreso/maximo-nivel', {
               method: 'POST',
               body: this.usuarioF.idUsuario,
               headers: {
                  'Content-Type': 'application/json'
               }
            }).then(res => res.json())
               .then(data => {
                  this.nivelMaximo = data.nivel;
                  this.niveles = new Array();
                  for (let i = 0; i <= Math.min(51, this.nivelMaximo); i++) {
                     this.niveles.push(i + 1);
                  }
               });
         },
         // Para obtener el historial de partidas
         obtenerPartidas: function () {
            fetch('progreso/puntajes', {
               method: 'POST',
               body: this.usuarioF.idUsuario
            }).then(res => res.json())
               .then(data => this.partidas = data);
            return true;
         },

         obtenerPuntajeTotal: function () {
            fetch('progreso/puntaje-total', {
               method: 'POST',
               body: this.usuarioF.idUsuario
            }).then(res => res.json())
               .then(data => this.puntajeTotal = data.puntajeTotal);
            return true;
         },

         obtenerUsuarioF: function () {
            let self = this;
            fetch('obtener-usuario', {
               method: 'POST',
               body: JSON.stringify(this.login),
               headers: {
                  'Content-Type': 'application/json'
               }
            }).then(res => res.json())
               .then(function (data) {
                  self.usuarioF = data;
                  if (self.usuarioF.idUsuario != null) {
                     self.mensaje="Bienvenido:  ";
                     self.mensaje=self.mensaje.concat(self.usuarioF.idUsuario);
                     self.isActive = true;
                     window.localStorage.setItem("user", JSON.stringify(data));
                     self.pagina = 4;
                     self.iniciar = true;
                  } else {
                     self.mensaje="Usuario o Contraseña Incorrecto";
                     self.isActive = true;
                  }
               });
         },
         obtenerRanking: function() {
            fetch('ranking', {
               method: 'POST'
            }).then(res => res.json())
               .then(data => {
                  let ord = 1;
                  this.ranks = data;
                  _.forEach(data, rank => {
                     rank.ord = ord++;
                  });
               });
            return true;
         },
         crearPartida: function (pg) {
            let self = this;
            fetch('crear-partida', {
               method: 'POST',
               body: JSON.stringify(this.usuarioF),
               headers: {
                  'Content-Type': 'application/json'
               }
            }).then(function (res) {
               return res.json();
            })
               .then(function (data) {
                  self.partida = data;
                  if (self.partida.idPartida != null) {
                     self.consultarNivelMaximo();
                     //  alert("Partida Creada\n El numero es: " + self.partida.idPartida);
                     self.pagina = pg;
                  } else {
                     alert("No se logró crear la partida");
                  }
               });
         },
         llenarPartida: function () {
            this.partida.puntaje = this.score;
            this.partida.nivel = this.nivel;
            let self = this;
            fetch('llenar-partida', {
               method: 'POST',
               body: JSON.stringify(this.partida),
               headers: {
                  'Content-Type': 'application/json'
               }
            }).then(res => res.json())
               .then(function (data) {
                  // self.partida = data;
                  console.log("Partida guardada: ", data);
               });
         },
         /**************************************************/
         setNivel: function (nivel) {
            this.nivel = nivel;
         },
         reiniciarJuego: function () {
            this.showSplash = false;
            let cartas = shuffleCards(this.nivel + 1);
            this.turns = 0;
            this.score = 0;
            this.started = false;
            this.startTime = 0;
            //cartas es la coleccion de cartas, y carta cada carta de la collecion
            _.each(cartas, function (carta) {
               //para cada carta de la coleccion se hace lo siguiente
               carta.volteada = false;
               carta.encontrada = false;
            });

            this.cartas = cartas;
         },

         siguienteNivel: function () {
            this.nivel++;
            this.crearPartida(5.2);
            this.reiniciarJuego();
         },
         cartasVolteadas: function () {
            return _.filter(this.cartas, function (carta) {//funcion que filtra cada carta
               return carta.volteada;//retornará las cartas que esten volteadas
            });
         },
         cartaVolteadaIgual() {
            let cartasVolteadas = this.cartasVolteadas();
            if (cartasVolteadas.length == 2) {
               if (cartasVolteadas[0].name == cartasVolteadas[1].name)
                  return true;
            }
         },
         setCartasEncontradas: function () {
            _.each(this.cartas, function (carta) {
               if (carta.volteada)
                  carta.encontrada = true;
            });
         },
         checkAllFound: function () {
            let cartasEncontradas = _.filter(this.cartas, function (carta) {
               return carta.encontrada;
            });
            if (cartasEncontradas.length == this.cartas.length)
               return true;
         },
         empezarJuego: function () {
            this.started = true;
            this.startTime = moment();//moment es un funcion de moment, se encarga del tiempo

            this.timer = setInterval(() => {
               this.time = moment(moment().diff(this.startTime)).format("mm:ss");
            }, 1000);
         },
         terminarJuego: function () {
            this.started = false;
            clearInterval(this.timer)
            let score;
            score = 1000 - (moment().diff(this.startTime, 'seconds') - this.nivel * 5) * 3 - (this.turns - this.nivel) * 5;

            this.score = Math.max(score, 0);
            this.llenarPartida(); // Guarda los datos de partida
            this.showSplash = true;
            this.startTime = 0;
         },
         voltearCarta: function (carta) {
            if (carta.encontrada || carta.volteada) return; // Si ya se volteo una carta durante el juego, la función termina

            // Determina el inicio de la partida
            if (!this.started) {
               this.empezarJuego();
            }

            let flipCount = this.cartasVolteadas().length;
            if (flipCount == 0) {
               carta.volteada = !carta.volteada; // carta.volteada toma el valor de V
            } else if (flipCount == 1) {
               carta.volteada = !carta.volteada;
               this.turns += 1;

               if (this.cartaVolteadaIgual()) {
                  // Match!
                  this.flipBackTimer = setTimeout(() => {
                     this.clearFlipBackTimer();
                     this.setCartasEncontradas();
                     this.clearFlips();

                     if (this.checkAllFound()) {
                        this.terminarJuego();
                     }
                  }, 200);
               } else {
                  // Wrong match
                  this.flipBackTimer = setTimeout(() => {
                     this.clearFlipBackTimer();
                     this.clearFlips();
                  }, 1000);
               }
            }
         },

         clearFlips: function () {
            _.map(this.cartas, function (carta) {
               return carta.volteada = false;
            });
         },

         clearFlipBackTimer: function () {
            clearTimeout(this.flipBackTimer);
            this.flipBackTimer = null;
         },
         jugar: function () {
            this.reiniciarJuego();
            this.setPagina(5.2)
         },
      },
      created: function () {
         this.reiniciarJuego();
      }
   }
);



document.addEventListener('DOMContentLoaded', () => {
   (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
      let $notification = $delete.parentNode;
      $delete.addEventListener('click', () => {
         $notification.parentNode.removeChild($notification);
      });
   });
});