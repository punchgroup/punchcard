package pe.edu.uni.fiis.punchCard.controller;

import pe.edu.uni.fiis.punchCard.model.Partida;
import pe.edu.uni.fiis.punchCard.model.UsuarioF;
import pe.edu.uni.fiis.punchCard.service.SingletonService;
import pe.edu.uni.fiis.punchCard.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "PartidaCrearController",urlPatterns = {"/crear-partida"})
public class PartidaCrearController extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = Json.getJson(req);
        UsuarioF usuarioF = Json.getInstance().readValue(data,UsuarioF.class);
        Partida partida= SingletonService.getPartidaService().crearPartida(usuarioF);
        Json.envioJson(partida,resp);
    }
}

