package pe.edu.uni.fiis.punchCard.dao.progreso;

import pe.edu.uni.fiis.punchCard.model.Datos;
import pe.edu.uni.fiis.punchCard.model.Partida;

import java.sql.Connection;
import java.util.List;


public interface ProgresoDao {
    Partida obtenerMaximoNivel(String idUsuario, Connection b);
    Datos obtenerPuntajeTotal(String idUsuario, Connection b);
    List<Partida> obtenerPuntajes(String idUsuario, Connection b);

    void guardarMaximoPuntaje(String idUsuario, Integer puntaje, Connection b);
    List<Datos> obtenerRanking(Connection b);
}
