package pe.edu.uni.fiis.punchCard.dao;

import pe.edu.uni.fiis.punchCard.dao.partida.PartidaDao;
import pe.edu.uni.fiis.punchCard.dao.partida.PartidaDaoImpl;
import pe.edu.uni.fiis.punchCard.dao.progreso.ProgresoDao;
import pe.edu.uni.fiis.punchCard.dao.progreso.ProgresoDaoImpl;
import pe.edu.uni.fiis.punchCard.dao.usuariof.UsuarioFDao;
import pe.edu.uni.fiis.punchCard.dao.usuariof.UsuarioFDaoImpl;

public abstract class SingletonDao {

    private static UsuarioFDao usuarioFDao = null;
    public static UsuarioFDao getUsuarioFDao(){
        if(usuarioFDao == null){
            usuarioFDao = new UsuarioFDaoImpl();
        }
        return usuarioFDao;
    }

    private static PartidaDao partidaDao =null;
    public static PartidaDao getPartidaDao(){
        if(partidaDao == null){
            partidaDao = new PartidaDaoImpl();
        }
        return partidaDao;
    }

    private static ProgresoDao progresoDao  = null;
    public static ProgresoDao getProgresoDao() {
        if (progresoDao == null) {
            progresoDao = new ProgresoDaoImpl();
        }
        return progresoDao;
    }

}