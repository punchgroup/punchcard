package pe.edu.uni.fiis.punchCard.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class ConsultarIdPartida {
    public static boolean consultarIdPartida(String idPartida, Connection b){
        boolean consult=true;
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("select id_partida from partida where id_partida=?)");
            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setString(1,idPartida);
            int d= sentencia.executeUpdate();
            consult=(d<=0);
            b.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return consult;
    }
}
