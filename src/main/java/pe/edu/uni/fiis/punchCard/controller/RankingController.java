package pe.edu.uni.fiis.punchCard.controller;

import pe.edu.uni.fiis.punchCard.service.SingletonService;
import pe.edu.uni.fiis.punchCard.service.progreso.ProgresoService;
import pe.edu.uni.fiis.punchCard.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "RankingController", urlPatterns = {"/ranking"})
public class RankingController extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProgresoService ps = SingletonService.getProgresoService();
        Json.getInstance();
        Json.envioJson(ps.obtenerRanking(), resp);
    }
}