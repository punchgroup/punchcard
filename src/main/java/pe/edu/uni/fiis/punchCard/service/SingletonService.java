package pe.edu.uni.fiis.punchCard.service;

import pe.edu.uni.fiis.punchCard.service.partida.PartidaService;
import pe.edu.uni.fiis.punchCard.service.partida.PartidaServiceImpl;
import pe.edu.uni.fiis.punchCard.service.progreso.ProgresoService;
import pe.edu.uni.fiis.punchCard.service.progreso.ProgresoServiceImpl;
import pe.edu.uni.fiis.punchCard.service.usuariof.UsuarioFService;
import pe.edu.uni.fiis.punchCard.service.usuariof.impl.UsuarioFServiceImpl;

public class SingletonService {

    private static UsuarioFService usuarioFService = null;

    public static UsuarioFService getUsuarioFService() {
        if (usuarioFService == null) {
            usuarioFService = new UsuarioFServiceImpl();
        }
        return usuarioFService;
    }

    private static PartidaService partidaService = null;

    public static PartidaService getPartidaService() {
        if (partidaService == null) {
            partidaService = new PartidaServiceImpl();
        }
        return partidaService;
    }

    private static ProgresoService progresoService = null;
    public static ProgresoService getProgresoService() {
        if (progresoService == null) {
            progresoService = new ProgresoServiceImpl();
        }
        return progresoService;
    }
}
