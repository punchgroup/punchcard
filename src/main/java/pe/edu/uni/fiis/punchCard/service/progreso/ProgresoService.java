package pe.edu.uni.fiis.punchCard.service.progreso;


import pe.edu.uni.fiis.punchCard.model.Datos;
import pe.edu.uni.fiis.punchCard.model.Partida;

import java.util.List;

public interface ProgresoService {
    Partida obtenerMaximoNivel(String idUsuario);
    List<Partida> obtenerPuntajes(String idUsuario);
    Datos obtenerPuntajeTotal(String idUsuario);
    List<Datos> obtenerRanking();
}
