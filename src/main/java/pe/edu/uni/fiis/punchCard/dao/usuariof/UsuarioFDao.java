package pe.edu.uni.fiis.punchCard.dao.usuariof;

import pe.edu.uni.fiis.punchCard.model.UsuarioF;

import java.sql.Connection;

public interface UsuarioFDao {
    public boolean agregarUsuarioF(UsuarioF a, Connection b);

    public UsuarioF obtenerUsuarioF(String correo, String password, Connection b);
}
