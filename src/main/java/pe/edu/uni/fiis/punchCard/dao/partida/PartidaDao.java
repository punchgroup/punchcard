package pe.edu.uni.fiis.punchCard.dao.partida;

import pe.edu.uni.fiis.punchCard.model.Partida;
import pe.edu.uni.fiis.punchCard.model.UsuarioF;

import java.sql.Connection;

public interface PartidaDao {
    public Partida crearPartida(UsuarioF a, Connection b);
    public Partida guardarPartida(Partida a, Connection b);
}
