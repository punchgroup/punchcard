package pe.edu.uni.fiis.punchCard.dao.partida;

import pe.edu.uni.fiis.punchCard.model.Partida;
import pe.edu.uni.fiis.punchCard.model.UsuarioF;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;

public class PartidaDaoImpl implements PartidaDao {

    public Partida crearPartida(UsuarioF a, Connection b) {
        Partida partida = new Partida();
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("insert into partida(id_partida,id_usuario, nivel, puntaje)" +
                    " values (?,?,null,0);");
            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            Random random = new Random();
            String idPartida;
            idPartida = String.valueOf(random.nextInt(10000));
            sentencia.setString(1, idPartida);
            sentencia.setString(2, a.getIdUsuario());
            sentencia.execute();
            partida.setIdPartida(idPartida);
            partida.setIdUsuario(a.getIdUsuario());
            partida.setNivel(null);
            partida.setPuntaje(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return partida;
    }

    public Partida guardarPartida(Partida a, Connection b) {
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("update partida set nivel=? , puntaje=? where id_partida=?");

            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setInt(1, a.getNivel());
            sentencia.setInt(2, a.getPuntaje());
            sentencia.setString(3, a.getIdPartida());
            sentencia.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return a;
    }

}
