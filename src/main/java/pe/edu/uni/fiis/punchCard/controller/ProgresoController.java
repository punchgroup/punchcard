package pe.edu.uni.fiis.punchCard.controller;

import pe.edu.uni.fiis.punchCard.service.SingletonService;
import pe.edu.uni.fiis.punchCard.service.progreso.ProgresoService;
import pe.edu.uni.fiis.punchCard.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "ProgresoController", urlPatterns = {"/progreso/*"})
public class ProgresoController extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String[] paths = req.getPathInfo().split("/");
        String operacion = paths[paths.length - 1];

        String idUsuario = Json.getJson(req);
        ProgresoService ps = SingletonService.getProgresoService();
        Json.getInstance();

        switch (operacion) {
            case "maximo-nivel":
                Json.envioJson(ps.obtenerMaximoNivel(idUsuario), resp);
                break;
            case "puntajes":
                Json.envioJson(ps.obtenerPuntajes(idUsuario), resp);
                break;
            case "puntaje-total":
                Json.envioJson(ps.obtenerPuntajeTotal(idUsuario), resp);
                break;
            default:
                resp.setStatus(404);
                resp.getWriter().write("Operacion invalida.");
        }
//        Partida partida = Json.getInstance().readValue(data,Partida.class);
//        partida= SingletonService.getPartidaService().guardarPartida(partida);
//        Json.envioJson(partida,resp);
    }
}

