package pe.edu.uni.fiis.punchCard.dao.progreso;

import pe.edu.uni.fiis.punchCard.model.Datos;
import pe.edu.uni.fiis.punchCard.model.Partida;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProgresoDaoImpl implements ProgresoDao {
    @Override
    public Partida obtenerMaximoNivel(String idUsuario, Connection b) {
        Partida partida = new Partida();
        partida.setIdUsuario(idUsuario);
        partida.setNivel(0);
        partida.setPuntaje(0);
        partida.setIdPartida("--");

        try {
            StringBuffer sql = new StringBuffer();
            sql.append("select max(nivel) nivel from partida where id_usuario = ? ;");
            PreparedStatement st = b.prepareStatement(sql.toString());
            st.setString(1, idUsuario);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                partida.setNivel(rs.getInt("nivel"));
            }
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return partida;
    }

    @Override
    public Datos obtenerPuntajeTotal(String idUsuario, Connection b) {
        Datos dt = new Datos();
        dt.setIdUsuario(idUsuario);
        dt.setPuntajeTotal(0);

        try {
            StringBuffer sb = new StringBuffer("select sum(xd.pnt) puntaje from (select max(puntaje) as pnt ");
            sb.append("from partida where id_usuario = ? group by nivel) as xd");

            PreparedStatement st = b.prepareStatement(sb.toString());
            st.setString(1, idUsuario);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                dt.setPuntajeTotal(rs.getInt("puntaje"));
            }
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return dt;
    }

    @Override
    public List<Partida> obtenerPuntajes(String idUsuario, Connection b) {
        List<Partida> ans = new ArrayList<>();
        try {


            StringBuffer sb = new StringBuffer("select id_partida, nivel, puntaje from partida ");
            sb.append("where id_usuario = ? and nivel notnull ");
            sb.append("order by nivel desc , puntaje desc ");

            PreparedStatement st = b.prepareStatement(sb.toString());
            st.setString(1, idUsuario);

            ResultSet rs = st.executeQuery();
            int ant = 100000;
            while (rs.next()) {
                Partida partida = new Partida();
                int level = rs.getInt("nivel");
                if (ant == level) continue;
                partida.setIdUsuario(idUsuario);
                partida.setNivel(rs.getInt("nivel"));
                partida.setPuntaje(rs.getInt("puntaje"));
                partida.setIdPartida(rs.getString("id_partida"));
                ans.add(partida);
                ant = level;
            }
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return ans;
    }

    @Override
    public void guardarMaximoPuntaje(String idUsuario, Integer puntaje, Connection b) {

        try {
            StringBuffer sql = new StringBuffer();
            sql.append("insert into datos(id_usuario, puntaje_total) values (?, ?) on conflict(id_usuario) do update set puntaje_total = ? ;");
            PreparedStatement ps = b.prepareStatement(sql.toString());
            ps.setString(1, idUsuario);
            ps.setInt(2, puntaje);
            ps.setInt(3, puntaje);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Datos> obtenerRanking(Connection b) {
        List<Datos> ans = new ArrayList<>();
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("select id_usuario, puntaje_total from datos order by puntaje_total desc limit(10)");
            PreparedStatement ps = b.prepareStatement(sql.toString());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Datos dt = new Datos();
                dt.setIdUsuario(rs.getString("id_usuario"));
                dt.setPuntajeTotal(rs.getInt("puntaje_total"));
                ans.add(dt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ans;
    }
}
