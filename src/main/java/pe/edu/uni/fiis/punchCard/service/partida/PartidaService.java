package pe.edu.uni.fiis.punchCard.service.partida;

import pe.edu.uni.fiis.punchCard.model.Partida;
import pe.edu.uni.fiis.punchCard.model.UsuarioF;

public interface PartidaService {
    public Partida crearPartida(UsuarioF usuarioF);
    public Partida llenarPartida(Partida partida);
}
