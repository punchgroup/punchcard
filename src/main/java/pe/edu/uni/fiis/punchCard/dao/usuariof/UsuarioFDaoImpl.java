package pe.edu.uni.fiis.punchCard.dao.usuariof;

import pe.edu.uni.fiis.punchCard.model.UsuarioF;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioFDaoImpl implements UsuarioFDao {
    public boolean agregarUsuarioF(UsuarioF a, Connection b) {
        boolean c=false;
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("insert into usuario(id_usuario,correo, password) values (")
                    .append("?,?,?);");
            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setString(1,a.getIdUsuario());
            sentencia.setString(2,a.getCorreo());
            sentencia.setString(3,a.getPassword());
            int d= sentencia.executeUpdate();
            c=d>0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return c;
    }
    public UsuarioF obtenerUsuarioF(String correo, String password, Connection b){
        UsuarioF usuarioF = new UsuarioF();
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("select id_usuario,correo,password from usuario where correo=? and password=?");
            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setString(1,correo);
            sentencia.setString(2,password);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                usuarioF.setIdUsuario(rs.getString("id_usuario"));
                usuarioF.setCorreo(rs.getString("correo"));
                usuarioF.setPassword(rs.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usuarioF;
    }

}