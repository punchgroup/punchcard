package pe.edu.uni.fiis.punchCard.controller;

import pe.edu.uni.fiis.punchCard.model.Login;
import pe.edu.uni.fiis.punchCard.model.UsuarioF;
import pe.edu.uni.fiis.punchCard.service.SingletonService;
import pe.edu.uni.fiis.punchCard.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UsuarioFObtenerController",urlPatterns = {"/obtener-usuario"})
public class UsuarioFObtenerController extends HttpServlet {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String data = Json.getJson(req);

        Login login = Json.getInstance().readValue(data,Login.class);
        UsuarioF usuarioF = SingletonService.getUsuarioFService().obtenerUsuarioF(login.getCorreo(),login.getPassword());

        Json.envioJson(usuarioF,resp);
    }
}
