package pe.edu.uni.fiis.punchCard.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class Conexion {
    public static Connection getConnection(){
        Connection conex = null;
        try {
            Class.forName("org.postgresql.Driver");
            //configurado para heroku
            conex = DriverManager.getConnection("jdbc:postgresql://ec2-174-129-227-128.compute-1.amazonaws.com:5432/d4epipjkth6cgd","whykonhkyaepue","178a07b4486655f85ec137f94ecae47dc691fbe18f0cfb97d4db15877af90a6f");
            conex.setAutoCommit(false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conex;
    }
}