package pe.edu.uni.fiis.punchCard.service.partida;

import pe.edu.uni.fiis.punchCard.dao.SingletonDao;
import pe.edu.uni.fiis.punchCard.model.Datos;
import pe.edu.uni.fiis.punchCard.model.Partida;
import pe.edu.uni.fiis.punchCard.model.UsuarioF;
import pe.edu.uni.fiis.punchCard.service.Conexion;

import java.sql.Connection;
import java.sql.SQLException;

public class PartidaServiceImpl implements PartidaService {
    public Partida crearPartida(UsuarioF usuarioF) {
        Connection connection= Conexion.getConnection();
        Partida usur = SingletonDao.getPartidaDao().crearPartida(usuarioF,connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  usur;
    }

    public Partida llenarPartida(Partida partida) {
        Connection connection= Conexion.getConnection();
        Partida part = SingletonDao.getPartidaDao().guardarPartida(partida, connection);

        Datos dt = SingletonDao.getProgresoDao().obtenerPuntajeTotal(partida.getIdUsuario(), connection);
        SingletonDao.getProgresoDao().guardarMaximoPuntaje(dt.getIdUsuario(), dt.getPuntajeTotal(), connection);

        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  part;
    }
}
