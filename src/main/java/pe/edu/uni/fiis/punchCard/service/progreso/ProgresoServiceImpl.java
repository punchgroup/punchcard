package pe.edu.uni.fiis.punchCard.service.progreso;

import pe.edu.uni.fiis.punchCard.dao.SingletonDao;
import pe.edu.uni.fiis.punchCard.model.Datos;
import pe.edu.uni.fiis.punchCard.model.Partida;
import pe.edu.uni.fiis.punchCard.service.Conexion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ProgresoServiceImpl implements ProgresoService {
    @Override
    public Partida obtenerMaximoNivel(String idUsuario) {
        Connection connection = Conexion.getConnection();

        Partida partida = SingletonDao.getProgresoDao().obtenerMaximoNivel(idUsuario, connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  partida;

    }

    @Override
    public List<Partida> obtenerPuntajes(String idUsuario) {
        Connection connection = Conexion.getConnection();
        List<Partida> partidaList = SingletonDao.getProgresoDao().obtenerPuntajes(idUsuario, connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  partidaList;
    }

    @Override
    public Datos obtenerPuntajeTotal(String idUsuario) {
        Connection connection = Conexion.getConnection();
        Datos datos = SingletonDao.getProgresoDao().obtenerPuntajeTotal(idUsuario, connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  datos;
    }

    @Override
    public List<Datos> obtenerRanking() {
        Connection connection = Conexion.getConnection();
        List<Datos> datosList = SingletonDao.getProgresoDao().obtenerRanking(connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  datosList;
    }
}
