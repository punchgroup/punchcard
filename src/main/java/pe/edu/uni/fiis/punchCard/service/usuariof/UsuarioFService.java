package pe.edu.uni.fiis.punchCard.service.usuariof;

import pe.edu.uni.fiis.punchCard.model.UsuarioF;

public interface UsuarioFService {
    public boolean agregarUsuarioF(UsuarioF usuariof);
    public UsuarioF obtenerUsuarioF(String correo, String password);
}
