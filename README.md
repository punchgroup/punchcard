Punch Card es un juego de cartas que consiste en encontrar el par de todas las cartas que se hayan elegido de acuerdo al nivel.


Una persona puede crear su cuenta o registrarse.


El usuario registrado puede:
1 jugar una partida
2 revisar su perfil
3 revisar su ranking en comparación con otros jugadores
4 cerrar su sesión en la sección de ajustes


Sobre las partidas:
1 Primero el jugador debe crear una partida
2 Luego seleccionar el nivel (por defecto será el nivel 1)
3 luego dar click en continuar
4 Empezar a jugar :v
5 al final de la partida se puede volver a jugar, pasar al siguiente nivel o volver al menu
6 Tambien al final se muestra el puntaje obtenido en la partida

Durante la partida se puede ver el tiempo de duración de la partida, los intentos y un boton para abandonar el juego


Sobre los niveles:
1 Jugar un nivel permite desbloquear un nuevo nivel
2 A mayor nivel se muestra mas cartas


Sobre los rankings
El ranking se forma con la suma de los mejores puntajes por cada nivel
Juegala aqui:
https://punchcard2.herokuapp.com/


